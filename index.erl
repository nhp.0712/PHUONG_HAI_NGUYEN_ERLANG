-module(index).
-compile(export_all).
-import(lists, [reverse/1]).

% Task #1: Write a function that prints the string “Hello,world.”
%				Task 1 
% =========================================
function1() ->
    io:format("Hello, World!~n~n").

% Task #2: Write one or more functions that use recursion to return the number of characters in a string.
%				Task 2 
%=========================================
function2([])     -> 0;
function2([_|T])  -> 1 + function2(T).

% Task #3: Write one or more functions that use recursion to return the number of words in a string.
%				Task 3 
%=========================================
function3([]) -> 0;
function3(Sentence) -> count(Sentence, 1).
 
count([], Count) -> Count;
count([32|Tail], Count) -> count(Tail, Count + 1);
count([_|Tail], Count) -> count(Tail, Count).

% Task #4: Write one or more functions that use recursion to count to N.
%				Task 4 
%=========================================
function4(Val, N) when Val < N ->
  io:fwrite("~w~n", [Val]),
  function4(Val + 1, N);
function4(_, N) ->
  io:fwrite("~w~n", [N]).
function4(N) ->
  function4(0, N).

% Task #5: Write one or more functions that use matching to selectively print “success” or “error: message” given input of the form {error, Message} or success.
%				Task 5 
%=========================================
function5(success) -> "success";
function5({error, Message}) -> "error: " ++ Message.

% Task #6: Consider a list of keyword - value tuples, such as [{erlang, a functional language}, {ruby, an OO language}]. 
% Write a function that accepts the list and a keyword and returns the associated value for the keyword. 
% Use a ﬁlter and list comprehension to complete this task.
%				Task 6 
%=========================================
function6(Key, List) -> search(Key, List).
search(Key, [{Key, Value}|_]) ->
    io:fwrite("Search 1 Keyword(~p) Value(~p)\n", [Key, Value]),
    Value;
search(Key, [_|T]) ->
    io:fwrite("Search 2 Keyword(~p) Tail(~p)\n", [Key, T]),
search(Key, T).

% Task #7: Implement a function to perform matrix multiplication. 
% The function should take two parameters – Matrix A and Matrix B – and return the resultant matrix – Matrix C. 
% Test by generating a variety of random matrices that are ﬁlled with random decimal values between 0 and 100. 
% You should test your code with matrix sizes of 50x50, 100x100, 500x500, and 1000x1000. You may not use any built-in libraries.
%				Task 7 
%=========================================
sumprod(0, _, _, Sum, _, _) -> Sum;
sumprod(I, C, R, Sum, M1, M2) -> 
    NewSum = Sum + (element(I,element(R,M1)) * element(C,element(I,M2))),
    sumprod(I-1, C, R, NewSum, M1, M2).

rowmult(_, 0, _, L, _, _) -> list_to_tuple(L);
rowmult(I, C, R, L, M1, M2) -> 
    SumProd = sumprod(I, C, R, 0, M1, M2),
    rowmult(I, C-1, R, [SumProd|L], M1, M2).

mmult(_, _, 0, MM, _, _) -> list_to_tuple(MM);
mmult(I, C, R, MM, M1, M2) ->
    NewRow = rowmult(I, C, R, [], M1, M2),
    mmult(I, C, R-1, [NewRow|MM], M1, M2).

mmult(M1, M2) -> 
    Inner = size(M2), % could optimize more by hardcoding the sizes
    NRows = size(M1), 
    mmult(Inner, NRows, NRows,[], M1, M2).

mkrow(0, L, Count) -> {list_to_tuple(reverse(L)), Count};
mkrow(N, L, Count) -> mkrow(N-1, [Count|L], Count+1).

mkmatrix(0, _, _, M) -> list_to_tuple(reverse(M));
mkmatrix(NR, NC, Count, M) ->
    {Row, NewCount} = mkrow(NC, [], Count),
    mkmatrix(NR-1, NC, NewCount, [Row|M]).

mkmatrix(NR, NC) -> mkmatrix(NR, NC, 1, []).

function7() ->
    Size = 5,
    MA = mkmatrix(Size, Size),
    MB = mkmatrix(Size, Size),
    MC = mmult(MA, MB),
    io:format("Matrix A: ~n"),
    A11 = element(1,element(1, MA)),
    A12 = element(1,element(2, MA)),
    A13 = element(1,element(3, MA)),
    A14 = element(1,element(4, MA)),
    A15 = element(1,element(5, MA)),
    io:fwrite("~w ~w ~w ~w ~w~n", [A11, A12, A13, A14, A15]),
    A21 = element(2,element(1, MA)),
    A22 = element(2,element(2, MA)),
    A23 = element(2,element(3, MA)),
    A24 = element(2,element(4, MA)),
    A25 = element(2,element(5, MA)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [A21, A22, A23, A24, A25]),
    A31 = element(3,element(1, MA)),
    A32 = element(3,element(2, MA)),
    A33 = element(3,element(3, MA)),
    A34 = element(3,element(4, MA)),
    A35 = element(3,element(5, MA)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [A31, A32, A33, A34, A35]),
    A41 = element(4,element(1, MA)),
    A42 = element(4,element(2, MA)),
    A43 = element(4,element(3, MA)),
    A44 = element(4,element(4, MA)),
    A45 = element(4,element(5, MA)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [A41, A42, A43, A44, A45]),
    A51 = element(5,element(1, MA)),
    A52 = element(5,element(2, MA)),
    A53 = element(5,element(3, MA)),
    A54 = element(5,element(4, MA)),
    A55 = element(5,element(5, MA)),  
    io:fwrite("~w ~w ~w ~w ~w~n~n", [A51, A52, A53, A54, A55]),

    io:format("Matrix B: ~n"),
    B11 = element(1,element(1, MB)),
    B12 = element(1,element(2, MB)),
    B13 = element(1,element(3, MB)),
    B14 = element(1,element(4, MB)),
    B15 = element(1,element(5, MB)),
    io:fwrite("~w ~w ~w ~w ~w~n", [B11, B12, B13, B14, B15]),
    B21 = element(2,element(1, MB)),
    B22 = element(2,element(2, MB)),
    B23 = element(2,element(3, MB)),
    B24 = element(2,element(4, MB)),
    B25 = element(2,element(5, MB)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [B21, B22, B23, B24, B25]),
    B31 = element(3,element(1, MB)),
    B32 = element(3,element(2, MB)),
    B33 = element(3,element(3, MB)),
    B34 = element(3,element(4, MB)),
    B35 = element(3,element(5, MB)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [B31, B32, B33, B34, B35]),
    B41 = element(4,element(1, MB)),
    B42 = element(4,element(2, MB)),
    B43 = element(4,element(3, MB)),
    B44 = element(4,element(4, MB)),
    B45 = element(4,element(5, MB)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [B41, B42, B43, B44, B45]),
    B51 = element(5,element(1, MB)),
    B52 = element(5,element(2, MB)),
    B53 = element(5,element(3, MB)),
    B54 = element(5,element(4, MB)),
    B55 = element(5,element(5, MB)),  
    io:fwrite("~w ~w ~w ~w ~w~n~n", [B51, B52, B53, B54, B55]),

     io:format("Matrix C: A * B ~n"),
    C11 = element(1,element(1, MC)),
    C12 = element(1,element(2, MC)),
    C13 = element(1,element(3, MC)),
    C14 = element(1,element(4, MC)),
    C15 = element(1,element(5, MC)),
    io:fwrite("~w ~w ~w ~w ~w~n", [C11, C12, C13, C14, C15]),
    C21 = element(2,element(1, MC)),
    C22 = element(2,element(2, MC)),
    C23 = element(2,element(3, MC)),
    C24 = element(2,element(4, MC)),
    C25 = element(2,element(5, MC)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [C21, C22, C23, C24, C25]),
    C31 = element(3,element(1, MC)),
    C32 = element(3,element(2, MC)),
    C33 = element(3,element(3, MC)),
    C34 = element(3,element(4, MC)),
    C35 = element(3,element(5, MC)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [C31, C32, C33, C34, C35]),
    C41 = element(4,element(1, MC)),
    C42 = element(4,element(2, MC)),
    C43 = element(4,element(3, MC)),
    C44 = element(4,element(4, MC)),
    C45 = element(4,element(5, MC)), 
    io:fwrite("~w ~w ~w ~w ~w~n", [C41, C42, C43, C44, C45]),
    C51 = element(5,element(1, MC)),
    C52 = element(5,element(2, MC)),
    C53 = element(5,element(3, MC)),
    C54 = element(5,element(4, MC)),
    C55 = element(5,element(5, MC)),  
    io:fwrite("~w ~w ~w ~w ~w~n", [C51, C52, C53, C54, C55]).
    

% Task #8: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. 
% Compare the computation time and lines of code with Function#5.
%				Task 8 
%=========================================
function8(1) -> io:format("~p~n",[[1]]);
function8(2) -> io:format("~p~n",[[1,1]]);
function8(N) when N > 2 -> 
    hd( fib(N-1,[1,1]) ).
fib(1,L) ->
    io:format("~p~n",[lists:reverse(L)]),
    L;
fib(N,L) ->
    [H1,H2|_T] = L,
    fib(N-1,[H1 + H2 | L]).

% Task #9: Write a function which starts 3 processes, and sends a message M times forwards and backwards between them. 
% For example, if M =2, then the ﬁrst process should create a message, send it to process #2 which will then forward it to process #3. 
% After process 3 recieves the message, it will send it back to process #2 which will forward the same message back to process #1. 
% When process #1 recieves the message, this pattern should repeat one more time. After the messages have been sent the processes should 
% terminate gracefully. Upon receiving a message, each process should output something along the lines of “Received Message#N” where N is
% the number of a given message.
%				Task 9 
%=========================================
function9() ->
    function9(10).

function9(NrMsgs) ->
    Pid1 = spawn(fun() -> process_1() end),
    spawn(fun() -> process_2(Pid1, NrMsgs) end),
    spawn(fun() -> process_3(Pid1, NrMsgs) end),
    ok.

process_1() ->
    io:format("Process 1 starts.~n"),
    process_1_loop(0),
    io:format("Process 1 ends.~n").

process_1_loop(MsgNr) ->
    receive
        {From, {message, More}} ->
            io:format("Process 1 received message #~p.~n", [MsgNr]),
            From ! {self(), {message_reply}},
            if
                More ->
                    process_1_loop(MsgNr+1);
                true ->
                    ok
            end
    end.

process_2(Pid1, NrMsgs) ->
    io:format("Process 2 starts.~n"),
    process_2_loop(Pid1, 1, NrMsgs),
    io:format("Process 2 ends.~n").

process_2_loop(Pid1, MsgNr, NrMsgs) ->
            io:format("Process 2 received message #~p.~n", [MsgNr]),
    More = (MsgNr < NrMsgs ),
    Pid1 ! {self(), {message, More}},
    receive
        {Pid1, {message_reply}} ->
            io:format("Process 2 received message #~p.~n", [MsgNr]),
            if
                More ->
                    process_2_loop(Pid1, MsgNr + 1, NrMsgs);
                true ->
                    ok
            end
    end.

process_3(Pid1, NrMsgs) ->
    io:format("Process 3 starts.~n"),
    process_3_loop(Pid1, 1, NrMsgs),
    io:format("Process 3 ends.~n").

process_3_loop(Pid1, MsgNr, NrMsgs) ->
            io:format("Process 3 received message #~p.~n", [MsgNr]),
    More2 = (MsgNr < NrMsgs),
    Pid1 ! {self(), {message, More2}},
    receive
        {Pid1, {message_reply}} ->
            io:format("Process 3 received message #~p.~n", [MsgNr]),
            if
                More2 ->
                    process_3_loop(Pid1, MsgNr + 1, NrMsgs);
                true ->
                    ok
            end
    end.


% Task #10: Write a function which starts N processes in a ring, and sends a message M times around all the processes in the ring. 
% When recieving a message, an individual process should output “Received Message M from N.” 
% After the messages have been sent the processes should terminate gracefully.
%				Task 10 
%=========================================
 function10() -> function10(3, 2).

function10(N, M) ->
    Pids = [spawn(fun() -> process_init(ProcessNr, N) end) || ProcessNr <- lists:seq(1, N)],
    [FirstPid | _] = Pids,
    set_next_pids(FirstPid, Pids),
    FirstPid ! {message, 1, M}.

set_next_pids(FirstPid, [Pid1, Pid2 | OtherPids]) ->
    Pid1 ! {set_next_pid, Pid2},
    set_next_pids(FirstPid, [Pid2 | OtherPids]);

set_next_pids(FirstPid, [LastPid]) ->
    LastPid ! {set_next_pid, FirstPid}.

process_init(ProcessNr, N) ->
    io:format("Process ~p: waiting for set_next_pid message~n", [ProcessNr]),
    receive
        {set_next_pid, NextPid} -> ok
    end,
    io:format("Process ~p: next pid is ~p~n", [ProcessNr, NextPid]),
    process_loop(ProcessNr, N, NextPid).

process_loop(ProcessNr, N, NextPid) ->
    receive
        {message, Loop, M} ->
            io:format("Process ~p: Received message ~p from ~p~n", [ProcessNr, Loop, NextPid]),
            if 
                ProcessNr == N ->
                    NewLoop = Loop + 1;
                true ->
                    NewLoop = Loop
            end,
            NextPid ! {message, NewLoop, M},
            if 
                Loop < M ->
                    process_loop(ProcessNr, N, NextPid);
                true ->
                    io:format("Process ~p: ends~n", [ProcessNr]),
                    ok
            end
    end.
