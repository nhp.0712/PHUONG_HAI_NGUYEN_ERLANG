1. Task #1: Write a function that prints the string “Hello,world.”
2. Task #2: Write one or more functions that use recursion to return the number of characters in a string.
3. Task #3: Write one or more functions that use recursion to return the number of words in a string.
4. Task #4: Write one or more functions that use recursion to count to N.
5. Task #5: Write one or more functions that use matching to selectively print “success” or “error: message” given input of the form {error, Message} or success.
6. Task #6: Consider a list of keyword - value tuples, such as [{erlang, a functional language}, {ruby, an OO language}]. 
    Write a function that accepts the list and a keyword and returns the associated value for the keyword. 
    Use a ﬁlter and list comprehension to complete this task.
7. Task #7: Implement a function to perform matrix multiplication. 
    The function should take two parameters – Matrix A and Matrix B – and return the resultant matrix – Matrix C. 
    Test by generating a variety of random matrices that are ﬁlled with random decimal values between 0 and 100. 
    You should test your code with matrix sizes of 50x50, 100x100, 500x500, and 1000x1000. You may not use any built-in libraries.
8. Task #8: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. 
    Compare the computation time and lines of code with Function#5.
9. Task #9: Write a function which starts 3 processes, and sends a message M times forwards and backwards between them. 
    For example, if M =2, then the ﬁrst process should create a message, send it to process #2 which will then forward it to process #3. 
    After process 3 recieves the message, it will send it back to process #2 which will forward the same message back to process #1. 
    When process #1 recieves the message, this pattern should repeat one more time. After the messages have been sent the processes should 
    terminate gracefully. Upon receiving a message, each process should output something along the lines of “Received Message#N” where N is
    the number of a given message.
10. Task #10: Write a function which starts N processes in a ring, and sends a message M times around all the processes in the ring. 
    When recieving a message, an individual process should output “Received Message M from N.” 
    After the messages have been sent the processes should terminate gracefully.


Instructions for compiling and running this Erlang file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_ERLANG
. Type this command to log in to Erlang: erl
. Test if the erl file has no error existing: c(FILE_NAME). (Ex: c(index).)
. Compile and run each function by command: FILE_NAME:FUNCTION_NAME(arg). (Ex: index:function1(). or index:function10(3,4).)
-> the solutions for all functions will show up as the example below

2. Using PowerShell in Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Type this command to log in to Erlang: erl
. Test if the erl file has no error existing: c(FILE_NAME). (Ex: c(index).)
. Compile and run each function by command: FILE_NAME:FUNCTION_NAME(arg). (Ex: index:function1(). or index:function10(3,4).)
-> the solutions for all functions will show up as the example below



                            Example Solutions for all functions
                            
//                          OPENING WINDOWS POWERSHELL  
// =============================================================================
Windows PowerShell
Copyright (C) 2014 Microsoft Corporation. All rights reserved.

PS C:\Users\ADMIN> cd C:\Users\ADMIN\CS3060_FA2017_A4_NGUYEN
PS C:\Users\ADMIN\CS3060_FA2017_A4_NGUYEN> erl
Eshell V9.1  (abort with ^G)

//                              TEST FILE - DEBUGGING  
// =============================================================================

1> c(index).
index.erl:2: Warning: export_all flag enabled - all functions will be exported
{ok,index}

//                          Run and compile Function 1  
// =============================================================================
2> index:function1().
Hello, World!

ok

//                          Run and compile Function 2  
// =============================================================================
3> index:function2("Erlang").
6

//                          Run and compile Function 3  
// =============================================================================
4> index:function3("Erlang is not fun at all").
6

//                          Run and compile Function 4  
// =============================================================================
5> index:function4(20).
0
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
ok

//                          Run and compile Function 5  
// =============================================================================
6> index:function5(success).
"success"
7> index:function5({error, "There exits an unknown error !!!"}).
"error: There exits an unknown error !!!"

//                          Run and compile Function 6  
// =============================================================================
8> index:function6(erlang,[{erlang,"a functional language"},{ruby,"an OO language"}]).
Search 1 Keyword(erlang) Value("a functional language")
"a functional language"
9> index:function6(ruby,[{erlang,"a functional language"},{ruby,"an OO language"}]).
Search 2 Keyword(ruby) Tail([{ruby,"an OO language"}])
Search 1 Keyword(ruby) Value("an OO language")
"an OO language"

//                          Run and compile Function 7  
// =============================================================================
10> index:function7().
Matrix A:
1 6 11 16 21
2 7 12 17 22
3 8 13 18 23
4 9 14 19 24
5 10 15 20 25

Matrix B:
1 6 11 16 21
2 7 12 17 22
3 8 13 18 23
4 9 14 19 24
5 10 15 20 25

Matrix C: A * B
215 490 765 1040 1315
230 530 830 1130 1430
245 570 895 1220 1545
260 610 960 1310 1660
275 650 1025 1400 1775
ok

//                          Run and compile Function 8  
// =============================================================================
11> index:function8(15).
[1,1,2,3,5,8,13,21,34,55,89,144,233,377,610]
610

//                          Run and compile Function 9  
// =============================================================================
12> index:function9(2).
Process 1 starts.
Process 2 starts.
Process 3 starts.
Process 2 received message #1.
Process 3 received message #1.
ok
Process 1 received message #0.
13> Process 1 received message #1.
13> Process 2 received message #1.
13> Process 3 received message #1.
13> Process 2 received message #2.
13> Process 3 received message #2.
13> Process 1 received message #2.
13> Process 1 ends.
13> Process 2 received message #2.
13> Process 2 ends.

//                          Run and compile Function 10  
// =============================================================================
13> index:function10(3,4).
Process 1: waiting for set_next_pid message
Process 2: waiting for set_next_pid message
Process 3: waiting for set_next_pid message
Process 1: next pid is <0.80.0>
Process 2: next pid is <0.81.0>
Process 3: next pid is <0.79.0>
{message,1,4}
Process 1: Received message 1 from <0.80.0>
14> Process 2: Received message 1 from <0.81.0>
14> Process 3: Received message 1 from <0.79.0>
14> Process 1: Received message 2 from <0.80.0>
14> Process 2: Received message 2 from <0.81.0>
14> Process 3: Received message 2 from <0.79.0>
14> Process 1: Received message 3 from <0.80.0>
14> Process 2: Received message 3 from <0.81.0>
14> Process 3: Received message 3 from <0.79.0>
14> Process 1: Received message 4 from <0.80.0>
14> Process 1: ends
14> Process 2: Received message 4 from <0.81.0>
14> Process 2: ends
14> Process 3: Received message 4 from <0.79.0>
14> Process 3: ends